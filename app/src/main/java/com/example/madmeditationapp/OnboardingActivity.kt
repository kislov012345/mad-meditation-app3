package com.example.madmeditationapp

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle

class OnboardingActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_onboarding)
    }

    fun reg(view: android.view.View){
        val registr = Intent(this, SignUpActivity::class.java)
        startActivity(registr)
    }
    fun login(view: android.view.View) {
        val log = Intent(this, SignInActivity::class.java)
        startActivity(log)
    }
}